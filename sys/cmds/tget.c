#include <stdio.h>
#include <iostream>
#include <string>
#include <curl/curl.h> //your directory may be different
using namespace std;

string data; //will hold the url's contents

size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up)
{
    for (int c = 0; c<size*nmemb; c++)
    {
        data.push_back(buf[c]);
    }
    return size*nmemb;
}

int sht_tget(char **args)
{
  //int argc = sizeof(args)/sizeof(*args);
  if (args[2] != args[3])
  {
    printf("sht: Error! To few arguments!\nsht: Usage:\n\ttget <URL> <FileNameToDownload>\n");
  }
  else
  {
    //HRESULT hr = URLDownloadToFile ( NULL, _T(args[1]), _T(args[2]), 0, NULL );

    //printf("sht: We are so sorry, but this function is disabled. Contact the creators.\n");

    CURL* curl; //our curl object

    curl_global_init(CURL_GLOBAL_ALL); //pretty obvious
    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_URL, args[1]);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    curl_easy_perform(curl);

    cout << endl << data << endl;
    cin.get();

    curl_easy_cleanup(curl);
    curl_global_cleanup();

    return 0;

  }

	return 1;
}
